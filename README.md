# Moravian Chronicles
An *Amnesia: The Dark Descent* Custom Story

I told you I will be coming back, didn't I?

## Roadmap

- ~~Finish the intro sequence~~
- ~~Finish first mansion level~~
- Finish the outside level
  - Add nooks and crannies with unlockable chests with goodies
  - Add locked doors to each building
  - Add entrances to other levels
- Boleslav mansion/house
- Taskforce HQ (use ptestbase for the main hall)
- Dungen level

## ModDB description

It is year 1887. Your name is Samuel Nistor, you are a 34-year-old writer whose script for his next article got stolen.
Venture into the mansion of Boleslav Opustil and find out the dark truth behind his story...

This is a short but sweet mod which doesn't aim to become a top mod.

Features:
- Uses the secret thaler mechanic
- Focus on using tinderboxes
- Nice design

### Mod idea, writing and credits song choice: WacksterPL

* All levels, all scripting, all custom assets, mod config, debugging and testing: Darkfire
* Additional barrel placement: SkinityGask
* Map design consulting: Maciek
* Custom music: other games and movies I like (copyright not intended)

INTENDED PROJECTS

> Note: use ~ instead of bullet points in the actual ModDB text

* Soul Asylum - the plot was supposed to take place in the 1990s in a Czech tyre factory. Further details were never considered.
* Moravian Chronicles 2: The Streets of Staryhrad - the plot is supposed to follow the story of Moravian Chronicels.
* Moravian Chronicles Remastered
* A sequel for Tornado of Souls - cancelled
* There are also intended mods with the main character Manfred Petersson, which are to follow the mod Beyond The Grave, so the whole series has to be created.


The entire series is set near Jihlava in western Moravy.
Each mod has its individual story, however they share the same universe, might share some characters or places and they all more or less intertwine or eventually amplify the fates of particular characters.

The story writer is WacksterPL and all the creators mentioned above are helping to make the stories happen.
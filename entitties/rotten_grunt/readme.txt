IF YOU'RE MAKING A MOD USING REMASTERED ASSETS:

- You have to use them inside the HPL2 engine.

- You MUST credit:

1. Me -- Florian (Normals, Speculars, Height Maps, Entities, Upscaled Textures, Sounds)
2. www.textures.com(Textures)
3. Mb3d.co.uk (Textures)
4. Cc0textures.com (Textures)
5. 3dassets.one (Textures)
6. Zapsplat.com (Sound Effects)


<SpawnParticleSystem

	ParticleSystem = "ps_water_rain.ps"
	
	HeightFromFeet = "5.0"
	HeightAddMin = "0.0"
	HeightAddMax = "0.0"
	
	Density = "0.08"
	
	Radius = "10"
	
	PSColor = "1 1 1 1.0"
	
	FadePS = "true"
	PSMinFadeEnd = "5"
	PSMinFadeStart = "8"
	PSMaxFadeStart = "17"
	PSMaxFadeEnd = "20"

/>
